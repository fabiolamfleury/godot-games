# Description

Repository with the objective of studying game development.

The games studied here are Flappy Bird and Fluffy Timber.

## Godot
Godot is an open source gaming language.

Godot comes with a nice IDE and you can test your game graphically or enter your code.

## Flappy Bird
The flappy bird game can be found in the folder called "Felpudo FLY". 

It's the game of a bird that must dodge certain obstacles by flying to survive. This game has achieved a lot of success in the years back.

## Timber Felpudo
The Timber Felpudo game is located in the folder called "Timber Plush". It's the game of a character who must hit barrels and dodge enemies.